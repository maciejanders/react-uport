// import { Connect } from 'uport-connect'

// export let uport = new Connect('TruffleBox')
// export const web3 = uport.getWeb3()

import { Connect, SimpleSigner } from 'uport-connect'
export let uport = new Connect('DAPP2', {
  clientId: '2oqr4wMJkkdJA4kWpESjgxHWVtrtwwubtHP',
  network: 'rinkeby',
  signer: SimpleSigner('12dda8c3e6293250d535542c8346946bf99d5e1e437caf9a5b3d0121ceda7d80')
})
export const web3 = uport.getWeb3()
